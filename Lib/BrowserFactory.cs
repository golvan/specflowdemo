﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NLog;

namespace SpecFlowDemo.Lib
{
    public class BrowserFactory
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static IWebDriver Driver { get; set; }

        public static void InitBrowser()
        {
            logger.Log(NLog.LogLevel.Info, "Starting browser::");
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            Driver = new ChromeDriver(options);
        }

        public static void GoTo(string url)
        {
            logger.Log(NLog.LogLevel.Info, string.Format("Opening url '{0}'::", url));
            Driver.Url = url;
        }

        public static void Teardown()
        {
            logger.Log(NLog.LogLevel.Info, "Closing browser.");
            Driver.Quit();
        }

        public static void MaximizeWindow()
        {
            Driver.Manage().Window.Maximize();
        }
    }
}
