﻿using OpenQA.Selenium;
using SpecFlowDemo.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace SpecFlowDemo.Helpers
{
    public static class SeleniumExtensions
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static bool IsElementPresent(this IWebDriver driver, By By)
        {
            bool result;
            try
            {
                driver.FindElement(By);
                result = true;
            }
            catch (NoSuchElementException)
            {
                result = false;
            }
            catch (WebDriverTimeoutException)
            {
                result = false;
            }
            return result;
        }

        public static List<string> GetElementsText(this IReadOnlyCollection<IWebElement> elements)
        {
            List<string> values = new List<string>();
            foreach (var element in elements)
            {
                var value = element.Text.Trim();
                if (!string.IsNullOrEmpty(value))
                {
                    values.Add(value);
                }
            }
            logger.Log(NLog.LogLevel.Debug, "Elements Text: '" + string.Join(", ", values) + "'");
            return values;
        }
    }
}
