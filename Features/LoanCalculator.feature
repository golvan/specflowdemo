﻿Feature: Loan Calculator

Scenario: Check page is opening
  Given that I have opened calculator page
  When page is loaded
  Then I should see 'Oblicz-Rate.pl' text in nav bar

Scenario: Check navigation elements
  Given that I have opened calculator page
  When page is loaded
  Then I should see 'Strona Główna, O Stronie, Kontakt, Kursy Walut' links in navigation
