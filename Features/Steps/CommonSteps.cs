﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlowDemo.Lib;
using SpecFlowDemo.PageObjects;
using System.Configuration;
using TechTalk.SpecFlow;
using NLog;

namespace SpecFlowDemo.Steps
{
    public class CommonSteps
    {
        protected IWebDriver driver;
        protected string url = ConfigurationManager.AppSettings["URL"];
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        [BeforeScenario()]
        public void TestSetup()
        {
            BrowserFactory.InitBrowser();
            driver = BrowserFactory.Driver;
        }

        [AfterScenario()]
        public void TestTeardown()
        {
            BrowserFactory.Teardown();
        }
    }
}
