﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlowDemo.Lib;
using SpecFlowDemo.PageObjects;
using SpecFlowDemo.Helpers;
using System.Configuration;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SpecFlowDemo.Steps
{
    [Binding]
    public class LoanCalculatorSteps : CommonSteps
    {

        [Given(@"that I have opened calculator page")]
        public void GivenThatIHaveOpenedCalculatorPage()
        {
            BrowserFactory.GoTo(url);
        }
        
        [When(@"page is loaded")]
        public void WhenPageIsLoaded()
        {
            HomePage homePage = new HomePage();
            Assert.IsTrue(homePage.IsPageTitleVisible());
        }
        
        [Then(@"I should see '(.*)' text in nav bar")]
        public void ThenIShouldSeeTextInNavBar(string expectedResult)
        {
            HomePage homePage = new HomePage();
            Assert.AreEqual(expectedResult, homePage.PageTitle.Text);
        }

        [Then(@"I should see '(.*)' links in navigation")]
        public void ThenIShouldSeeLinksInNavigation(string items)
        {
            List<string> expItems = items.Split(',').Select(item => item.Trim()).ToList();
            HomePage homePage = new HomePage();
            var actualItems = homePage.NavigationList.GetElementsText();
            CollectionAssert.AreEqual(expItems, actualItems, "Actual values '" + string.Join(", ", actualItems) + "' did not match expected values.'");
            logger.Log(NLog.LogLevel.Info, "Assert Success. Actual values matched expected values.");
        }
    }
}
