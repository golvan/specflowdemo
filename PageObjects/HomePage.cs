﻿using OpenQA.Selenium;
using SpecFlowDemo.Helpers;
using System.Collections.Generic;

namespace SpecFlowDemo.PageObjects
{
    public class HomePage : BasePage
    {
        public IWebElement PageTitle
        {
            get
            {
                return driver.FindElement(By.ClassName("navbar-brand"));
            }
        }

        public IReadOnlyCollection<IWebElement> NavigationList
        {
            get
            {
                return driver.FindElements(By.CssSelector(".nav.navbar-nav:not(.navbar-right) li"));
            }
        }

        public bool IsPageTitleVisible()
        {
            return driver.IsElementPresent(By.ClassName("navbar-brand"));
        }
    }
}
