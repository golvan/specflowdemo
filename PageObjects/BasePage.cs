﻿using NLog;
using OpenQA.Selenium;
using SpecFlowDemo.Helpers;
using SpecFlowDemo.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowDemo.PageObjects
{
    public class BasePage
    {
        protected IWebDriver driver = BrowserFactory.Driver;
        protected Logger logger = LogManager.GetCurrentClassLogger();
    }
}
